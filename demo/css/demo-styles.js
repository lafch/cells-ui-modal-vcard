import { setDocumentCustomStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }
  .footer {
    padding: 20px 0 10px;
  }
  

  h2 {
    margin-top: 0;
  }

  .cells-modal-body {
    padding: 20px;
    font-size: 13px;
    line-height: 1.25em;
    overflow-y: auto;
    max-height: 30rem;
  }
  .cells-modal-footer {
    padding: 0 20px 20px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  bbva-button-default {
    margin-right:5px;
  }

`);
