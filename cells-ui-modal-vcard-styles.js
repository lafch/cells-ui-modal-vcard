import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-ui-modal-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

.animate, .close:hover, .cells-modal-overlay, .cells-modal-content, .cells-modal.opened .cells-modal-overlay, .cells-modal.opened .cells-modal-content {
  transition-property: all;
  transition-duration: 0.3s;
  transition-timing-function: ease-in-out; }

.delayed, .cells-modal.opened .cells-modal-content {
  transition-delay: 0.5s; }

.close {
  background: #fff;
  border: none;
  color: #666;
  position: absolute;
  top: 0;
  right: 0;
  cursor: pointer;
  padding: 15px 18px; }
  .close:hover {
    background: #eee;
    color: #444; }

.cells-modal {
  position: fixed;
  top: 0;
  width: 100%;
  height: 100vh;
  z-index: 1;
  visibility: hidden; }
  .cells-modal-overlay {
    position: absolute;
    background: rgba(0, 0, 0, 0.35);
    width: 100%;
    height: 100%;
    z-index: 10;
    opacity: 0; }
  .cells-modal-content {
    position: absolute;
    z-index: 100;
    background: #fff;
    top: 50%;
    left: 50%;
    opacity: 0;
    transform: translate(-50%, -20%);
    box-shadow: 0 0 13px rgba(0, 0, 0, 0.35);
    color: #3b3b3b; }
  .cells-modal-header {
    background: #fff;
    height: 47px;
    padding: 0 20px;
    border-bottom: 1px solid #ddd; }
    .cells-modal-header h2 {
      margin: 0;
      color: #1973B8;
      font-size: 13px;
      text-transform: uppercase;
      font-weight: bolder;
      padding: 16px 0; }
  .cells-modal-body {
    padding: 20px;
    background: rgba(0, 0, 0, 0.015);
    font-size: 13px;
    line-height: 1.25em;
    overflow-y: auto;
    max-height: 30rem; }
  .cells-modal-footer {
    padding: 0 20px 20px;
    background: rgba(0, 0, 0, 0.015); }
  .cells-modal.opened {
    z-index: 5000;
    visibility: visible; }
    .cells-modal.opened .cells-modal-overlay {
      opacity: 1; }
    .cells-modal.opened .cells-modal-content {
      opacity: 1;
      transform: translate(-50%, -50%); }

.message {
  display: grid;
  padding: 15px 15px 30px;
  align-items: center;
  grid-template-columns: 1fr 4fr; }
  .message .icon {
    text-align: center; }
  .message .text {
    line-height: 1.28rem;
    font-size: 15px; }
  .message.info cells-icon {
    color: #1973B8; }
  .message.warning cells-icon {
    color: #F7893B; }
  .message.error cells-icon {
    color: #DA3851; }
  .message.success cells-icon {
    color: #2bbbad; }
  .message bbva-button-default {
    margin: 10px 0; }

bbva-button-default {
  --bbva-button-default-min-width: 6.75rem;
  --bbva-button-default-font-size: 0.825rem;
  min-height: 2.4rem; }
`;