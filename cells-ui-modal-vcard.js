import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-modal-vcard-styles.js';
import '@bbva-web-components/bbva-button-default';
import '@cells-components/cells-icon/cells-icon.js';
import '@vcard-components/cells-util-behavior-vcard';

const cellsUtilBehaviorVcard = CellsBehaviors.cellsUtilBehaviorVcard;
/**
This component ...

Example:

```html
<cells-ui-modal-vcard></cells-ui-modal-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsUiModalVcard extends cellsUtilBehaviorVcard(LitElement) {
  static get is() {
    return 'cells-ui-modal-vcard';
  }


  // Declare properties
  static get properties() {
    return {
      title: {type: 'String'},
      type: {type: 'String'},
      icon: {type: 'String'},
      message: {type: 'String'},
      buttonCancel: {type: 'String'},
      buttonOk: {type: 'String'},
      width: {type: 'String'},
      _listIcons: {type: 'Object'},
      onOk: { type: Object},
      onCancel: { type: Object}
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.title = '';
    this.type = ''; // info, warning, error, success
    this.icon = '';
    this.message = '';
    this.buttonCancel = 'Cancelar';
    this.buttonOk = 'Aceptar';
    this.width = '420px';
    this._listIcons = {
      'info': 'info',
      'warning': 'alert',
      'error': 'error',
      'success': 'correct'
    };
  }

  open(onOk, onCancel) {
    this.onCancel = onCancel;
    this.onOk = onOk;
    this.shadowRoot.querySelector('#modal').classList.add('opened');
  }
  
  close() {
    this.onCancel = null;
    this.onOk = null;
    this.shadowRoot.querySelector('#modal').classList.remove('opened');
    this.dispatch(this.events.onCloseModal, {idModal: this.id});
  }

  _handleCancel() {
    if(this.onCancel){
      this.onCancel();
    }
    this.dispatch(this.events.onCancelModal, {idModal: this.id});
    this.close();
  }

  _handleOk() {
    if(this.onOk){
      this.onOk();
    }
    this.dispatch(this.events.onOkModal, {idModal: this.id});
    this.close();
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-quotation-report-pfco-shared-styles').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>

      <div class="cells-modal" id="modal">
        <div class="cells-modal-overlay"></div>
        <div class="cells-modal-content" style="width: ${this.width}">
          <button class="close" @click="${this.close}">X</button>
          <div class="cells-modal-header">
            <h2>${this.title}</h2>
          </div>

          ${this.type ? html`
            <div class="message ${this.type}">
              <div class="icon"><cells-icon icon="coronita:${this.icon ? this.icon : this._listIcons[this.type]}" size="48"></cells-icon></div>
              <div class="text">${this.message}</div>
              <div></div>
              <div style = "margin-right: 35px;text-align: center;">
                ${this.buttonCancel ? html`
                  <bbva-button-default text="${this.buttonCancel}" @click="${this._handleCancel}" class="secondary"></bbva-button-default>` : '' }
                ${this.buttonOk ? html`
                  <bbva-button-default text="${this.buttonOk}" @click="${this._handleOk}"></bbva-button-default>` : '' }
              </div>
            </div>
            ` : html`<slot></slot>` }
        </div>
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiModalVcard.is, CellsUiModalVcard);
